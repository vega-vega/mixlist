package com.vegzul.mixlist;

import com.vegzul.mixlist.iface.ReadList;
import com.vegzul.mixlist.model.DataModel;

import javax.json.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vasily.bolgasov on 22.05.2017.
 */
final public class ReadListData implements ReadList {

    private List<DataModel> dataModelList = null;

    public void readJson(String fileData) {
        dataModelList = new ArrayList<DataModel>();
        JsonArray jsonArray = readJsonValues(fileData);

        for (JsonValue jsonValue : jsonArray) {
            JsonObject jsonObject = (JsonObject) jsonValue;
            DataModel dataModel = new DataModel(jsonObject.size());

            for (JsonValue value : jsonObject.values()) {
                dataModel.addData(value.toString());
            }

            dataModelList.add(dataModel);
        }
    }

    public void mixListData() {
        if (dataModelList != null) {
            Collections.shuffle(dataModelList);
        }
    }

    public List<DataModel> getDataModelList() {
        return dataModelList;
    }

    private JsonArray readJsonValues(String fileData) {
        JsonArray jsonValues = null;
        InputStream inputStream = null;
        JsonReader jsonReader = null;

        try {
            if (new File(fileData).exists()) {
                inputStream = new FileInputStream(fileData);
            } else {
                inputStream = new ByteArrayInputStream(fileData.getBytes("UTF-8"));
            }

            jsonReader = Json.createReader(inputStream);

            jsonValues = jsonReader.readArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (jsonReader != null) {
                jsonReader.close();
            }
        }

        return jsonValues;
    }
}

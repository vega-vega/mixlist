package com.vegzul.mixlist.iface;

import com.vegzul.mixlist.model.DataModel;

import java.util.List;

/**
 * Created by vasily.bolgasov on 22.05.2017.
 */
public interface ReadList {

    void readJson(String filePath);

    void mixListData();

    List<DataModel> getDataModelList();
}

package com.vegzul.mixlist.model;

import java.util.Arrays;

/**
 * Created by vasily.bolgasov on 22.05.2017.
 */
public class DataModel {

    private int length = 0;
    private String[] data;
    private int last = 0;

    public DataModel(int length) {
        this.length = length;
        data = new String[length];
    }

    public boolean addData(String value) {
        return addData(value, false);
    }

    public boolean addData(String value, boolean update) {
        if (update && last >= length) {
            last = 0;
        }

        if (last < length) {
            data[last] = value;
            last++;

            return true;
        }

        return false;
    }

    public String getData(int position) {
        if (position >= 0 && position < length) {
            return data[position];
        }

        return null;
    }

    public String[] getArray() {
        return data;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "length=" + length +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}

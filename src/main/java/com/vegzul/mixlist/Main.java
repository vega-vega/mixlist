package com.vegzul.mixlist;

import com.vegzul.mixlist.iface.ReadList;
import com.vegzul.mixlist.model.DataModel;

import java.net.URL;

/**
 * Created by vasily.bolgasov on 22.05.2017.
 */
public class Main {

    public static void main(String[] args) {
        ReadList readList = new ReadListData();
        URL url = Main.class.getClassLoader().getResource("test.json");
        readList.readJson(url.getFile());

        for (DataModel dataModel : readList.getDataModelList()) {
            System.out.println(dataModel);
        }

        System.out.println(" - - - ");

        readList.mixListData();
        for (DataModel dataModel : readList.getDataModelList()) {
            System.out.println(dataModel);
        }
    }
}
